<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Registro de  VM</title>
    <style>
      body
      {
        background-image: url(all-of-those-images/interf/logoazul.png);
        background-repeat: no-repeat;
        background-size: 240px 68px;
        background-position: 96% 25px;
      }
      th
      {
        font-size: 20px;
        font-weight: bold;
        border: 1px flat #000033;
      }
      .shad
      {
        font-size: 20px;
        font-weight: bold;
        border: 1px flat #000033;
      }
      button,input[type=submit],input[type=reset]
      {
        background-color: #D6EAF8;
        padding: 4px 4px;
        border: outset #ABB2B9;
        cursor: pointer;
        font-size: 15px;
        font-weight: bold;
        box-shadow: 2px 3px 10px #000033;
      }
      .evilbtn
      {
        display: inline;
        background-color: #F7DC6F;
        border:  double #FCF3CF;
        font-size: 15px;
        font-weight: bold;
        float: right;
        margin-right: 5%;
        cursor: default;
        border-radius: 50%;
        height: 60px;
      }
      .containerOfRod
      {
        padding: 10px ;
        display: inline;
        width: 100%;
      }
      .unselectable
      {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
    </style>
    <script language="JavaScript">
      var countmachine = 0;
      var dba = new Array();
      var soa = new Array();
      function cerrar(obj)
      {
        linea=document.getElementById("linea");
        linea.parentNode.removeChild(linea.parentNode.childNodes[countmachine+3]);
        countmachine--;
        document.getElementById("count1").value=countmachine;
        if(countmachine==0)
        {
            document.getElementById("campo").removeChild(document.getElementById("referencia1"));
        }
      }
      function nuevoNodo(machineNumber,thatName)
      {
        machineNumber=machineNumber-1;
        esteInput1 = document.createElement("input");
        esteInput1.name="cpu"+machineNumber;
        esteInput1.type="number";
        esteInput1.autocomplete="off";
        esteInput1.maxLength="3";
        esteInput1.oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);";
        esteInput2 = document.createElement("input");
        esteInput2.name="mem"+machineNumber;
        esteInput2.type="number";
        esteInput2.autocomplete="off";
        esteInput2.maxLength="3";
        esteInput2.oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);";
        esteInput3 = document.createElement("input");
        esteInput3.name="dis"+machineNumber;
        esteInput3.type="number";
        esteInput3.autocomplete="off";
        esteInput3.maxLength="3";
        esteInput3.oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);";
        esteInput4 = document.createElement("select");
        esteInput4.name="so"+machineNumber;
        esteInput4.autocomplete="off";
        esteInput5 = document.createElement("select");
        esteInput5.autocomplete="off";
        esteInput5.name="db"+machineNumber;
        ThaNode = document.createElement("tr");
        ThaNode.appendChild(document.createElement("td"));
        ThaNode.appendChild(document.createElement("td"));
        ThaNode.appendChild(document.createElement("td"));
        ThaNode.appendChild(document.createElement("td"));
        ThaNode.appendChild(document.createElement("td"));
        ThaNode.appendChild(document.createElement("td"));
        ThaNode.firstChild.appendChild(document.createTextNode(thatName));
        ThaNode.childNodes[1].appendChild(esteInput1);
        ThaNode.childNodes[2].appendChild(esteInput2);
        ThaNode.childNodes[3].appendChild(esteInput3);
        ThaNode.childNodes[4].appendChild(esteInput4);
        ThaNode.childNodes[5].appendChild(esteInput5);
        for (var i = 0; i < soa.length; i++)
        {
          var option = document.createElement("option");
          option.value = soa[i];
          option.text = soa[i];
          esteInput4.appendChild(option);
        }
        for (var i = 0; i < dba.length; i++)
        {
          var option = document.createElement("option");
          option.value = dba[i];
          option.text = dba[i];
          esteInput5.appendChild(option);
        }
        return ThaNode;
      }
      function nuevavm()
      {
        countmachine++;
        document.getElementById("count1").value=countmachine;
        linea=document.getElementById("linea");
        othername=countmachine+1;
        tree=nuevoNodo(othername,othername+":");
        linea.parentNode.insertBefore(tree,linea);
        if(countmachine==1)
        {
          thisKiller = document.createElement("a");
          thisKiller.id="referencia1";
          thisKiller.href="javascript:cerrar(this)";
          thisKiller.appendChild(document.createTextNode("Borrar linea"));
          document.getElementById("campo").appendChild(thisKiller);
        }
      }
    </script>
    <?php include 'dbc.php';?>
  </head>
  <body>
  <div style="border:10px groove #616161;border-radius: 10px; ">
    <form method="post"  action="inthevm.php"> <br>
      <br>
      <div class="containerOfRod" >
        <input type="submit">
        <input type="reset">
      </div>
      <div class="shad" align="center">
      Disco compartido (spec):<input type="text" value="0" name="sdis" autocomplete="off" >
      </div>
      <br><br>
      <?php
        $sql = 'select FR2  from filtro order by FR2 desc limit 1';
        $conn = mysqli_connect($host,$user,$pass,$db);
        $re = mysqli_query($conn,$sql);
        if(!$re)
          echo "Conexion con BD fallida";
        else
        {
           $row = mysqli_fetch_array($re);
           $rrr=$row['FR2']+1;
           echo "<input type=\"hidden\" name=\"rrr\" value=\"".$rrr."\" >";
           echo "<input type=\"hidden\" name=\"count1\" id=\"count1\" value=\"\">";
        }
      ?>
      <table align="center">
        <tr>
          <th>No:</th>
          <th>vCPUs:</th>
          <th>Memoria (gb):</th>
          <th>Disco (gb):</th>
          <th>SO:</th>
          <th>Base De Datos:</th>
        </tr>
        <tr>
          <td>1:</td>
          <td><input type="number" name="cpu0" autocomplete="off"  maxlength="3"  oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" ></td>
          <td><input type="number" name="mem0" autocomplete="off"  maxlength="3"  oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"></td>
          <td><input type="number" name="dis0" autocomplete="off"  maxlength="6"  oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"></td>
          <td>
          <select name="so0" autocomplete="off">
            <option value=""></option>
            <?php
              $re = mysqli_query($conn,"select nombre from SO");
              if(! $re)
                echo "<option value=\"Pendiente\">Pendiente</option> ";
              else
              {
                echo "<script >soa[0]=\"\";</script>";
                $s=1;
                while($row = mysqli_fetch_array($re))
                {
                  echo "<script >soa[".$s."]='".$row['nombre']."';</script>";
                  $s++;
                  $o ="<option value=\"".$row['nombre']."\">".$row['nombre']."</option>";
                  echo $o;
                }
              }
            ?>
          </select>
          </td>
          <td>
          <select name="db0" autocomplete="off">
            <option value="N/A">N/A</option>
            <?php
              $re = mysqli_query($conn,"select nombre from datab");
              if(! $re)
                echo "<option value=\"Pendiente\">Pendiente</option> ";
              else
              {
                $s=0;
                while($row = mysqli_fetch_array($re))
                {
                  echo "<script >dba[".$s."]='".$row['nombre']."';</script>";
                  $s++;
                  $o ="<option value=\"".$row['nombre']."\">".$row['nombre']."</option>";
                  echo $o;
                }
              }
              mysqli_close($conn);
            ?>
          </select>
          </td>
        </tr>
        <tr id="linea">
          <td id="campo"><a href="javascript:nuevavm();">Agregar linea</a>&nbsp;</td>
        </tr>
      </table>
    </form>
  </div>
  </body>
</html>