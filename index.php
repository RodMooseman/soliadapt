<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Inicio</title>
    <style>
      input[type=submit]
      {
        background-color: #D6EAF8;
        padding: 4px 4px;
        border: outset #ABB2B9;
        cursor: pointer;
        font-size: 15px;
        font-weight: bold;
        box-shadow: 2px 3px 10px #000033;
      }
      .container
      {
        padding: 4px 4px;
        box-sizing: border-box;
        font-size: 20px;
        border:10px groove #616161;
        border-radius: 25px;
      }
    </style>
    <?php include 'dbc.php'; ?>
  </head>
  <body>
    <div class="container" align="center">
      <form action='reg.php' ><br>
        <input type='submit' value='Registrar nueva solicitud'>
      </form>
      <form action='moodthis.php' ><br>
        <select name="chooseu" autocomplete="off">
          <option value=""></option>
          <?php
            $conn = mysqli_connect($host, $user, $pass, $db);
            $re = mysqli_query($conn,"select Pro,FR2 from filtro");//where Sol=user (cuando se tenga Bd user)
            if(! $re)
              echo "<option value=\"\">Sin conexion</option> ";
            else
            {
              while($row = mysqli_fetch_array($re))
              {
                $o ="<option value=\"".$row['FR2']."\">".$row['Pro']."</option>";
                echo $o;
              }
            }
          ?>
        </select>
        <input type='submit' value='Modificar Solicitud'>
      </form>
    </div>
  </body>
</html>