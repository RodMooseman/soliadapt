<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Registro de Proyectos</title>
  <style>
    body
    {
      background-image: url(all-of-those-images/interf/logoazul.png);
      background-repeat: no-repeat;
      background-size: 260px 70px;
      background-position: 96% 25px;
    }
    /*tamaño de casillas*/
    #FR,#FR2,#Pro,#ASol,#Sol,#Ger,#Dir,#PN,#DP,#PP,#TEI,#DA,#Com,#HO,#NU,#Ing,#DR,#Sta
    {
      width:120px;
      float:right;
      margin-right:40px;
    }
    button,input[type=submit],input[type=reset]
    {
      background-color: #D6EAF8;
      padding: 4px 4px;
      border: outset #ABB2B9;
      cursor: pointer;
      font-size: 15px;
      font-weight: bold;
      box-shadow: 2px 3px 10px #000033;
    }
    .evilbtn
    {
      display: inline;
      background-color: #F7DC6F;
      border:  double #FCF3CF;
      font-size: 15px;
      font-weight: bold;
      float: right;
      margin-right: 5%;
      cursor: default;
      border-radius: 50%;
      height: 60px;
    }
    .container
    {
      padding: 4px 4px;
      box-sizing: border-box;
      font-size: 14px;
    }
    .container2, .container3
    {
      padding: 6px ;
      display: inline-block;
      width: 100%;
    }
    form { display: inline; }
    .DaBox
    {
        width: 50%;
      margin-right:30px;
      margin-left:30px;
      border:5px groove #102b3d;
      border-radius: 25px;
      text-shadow: 1px 1px 5px #5DADE2;
      #background: #d0cbd6;
    }
    .line1 , .line2 , .line3
    {
      float: left;
      margin-left:5px;
      display:inline;
    }
    .line1
    {
      text-shadow: 1px 1px 5px #151f6b;
      #background: #d0cbd6;
      width: 28%;
    }
    .line2
    {
    /*background: #999999;*/
      width: 33%;
    }
    .line3
    {
      text-shadow: 1px 1px 5px #151f6b;
      #background: #d0cbd6;
      width: 33%;
    }
    .unselectable
    {
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }
  </style>
  <?php
    include 'dbc.php';
    $thatData = array('Pro' => '','ASol' => '','Sol' => '','Ger' => '','Dir' => '','PN' => 'Si','DP' => '','PP' => '','TEI' => '','DA' => '','Com' => '','Sta' => 'Pendiente','HO' => '','NU' => '','Ing' => '','DR' => '');
    $otherData = array('Pro','ASol','Sol','Ger','Dir','DP','PP','TEI','DA','Com','PN','Sta','HO','NU','Ing','DR');
    $goal= array('Proyecto','Ambiente solicitado','Solicitante','Gerencia','Direccion','Descripcion del proyecto','Provedor del proyecto','Tiempo estimado de implementacion','Diagrama de arquitectura');
    if($_SERVER['REQUEST_METHOD']=="POST"&&isset($_POST['iwantmachines']))
    {
      $fl="";
      
      for($i=0;$i<16;$i++)
      {
        $thatData[$otherData[$i]]=antihack($_POST[$otherData[$i]]);
        if (empty($_POST[$otherData[$i]])&&$i<8)
         $fl .= " \\n El campo ".$goal[$i]." no puede estar vacio";
      }
      $sql = "select FR2  from filtro order by FR2 desc limit 1";
      $conn = mysqli_connect($host,$user,$pass,$db);
      $re = mysqli_query($conn,$sql);
      if(!$re)
        echo "Conexion con BD fallida".mysqli_error();
      else
      {
        $row = mysqli_fetch_array($re);
        $fr2=$row['FR2']+1;
      }
      $isupp=0;
      if($fl=="")
      {
        unset($row);
        unset($re);
        $sql = "select No from  filtmachine where No=".$fr2;
        $re = mysqli_query($conn,$sql);
        if(!$re)
          echo "Conexion con BD fallida".mysqli_error();
        else
        {
          while($row = mysqli_fetch_array($re))
          if($row['No'])
            $isup++;
          if($isup==0)
            $fl .= "\\n Ninguna maquina registrada en esta solicitud ";
          else
          {
            $nimg = $_FILES['DA']['name'];
            $timg = $_FILES['DA']['type'];
            if ($nimg == !NULL) 
            {
              if(($timg == "image/jpeg") || ($timg == "image/jpg") || ($timg == "image/png"))
              {
                if($timg == "image/jpeg")
                {
                  $add="all-of-those-images/fil/$fr2.jpeg";
                  $thatData[$otherData[8]]="jpeg";
                }
                if($timg == "image/jpg")
                {
                  $add="all-of-those-images/fil/$fr2.jpg";
                  $thatData[$otherData[8]]="jpg";
                }
                if($timg == "image/png")
                {
                  $add="all-of-those-images/fil/$fr2.png";
                  $thatData[$otherData[8]]="png";
                }
                if(!move_uploaded_file($_FILES['DA']['tmp_name'],$add))
                {
                  echo "<script type=\"text/javascript\">alert(\"Error en la copia de imagen \");</script>";
                  $isup=-99;
                }  
              }
              else
                {
                  $fl="Formato no permitido";
                  $isup=-99;
                }
            }
            else 
            {
              $fl="Ninguna imagen detectada en Diagrama de arquitectura";
              $isup=-99;
            }  
          }
        }
      }
      if($isup>0)
      {
        $time= new DateTime();
        $time=$time->format('Y-m-d');
        $sql = "insert into filtro values ('".$time."',".$fr2.",'".$thatData[$otherData[0]]."','".$thatData[$otherData[1]]."','".$thatData[$otherData[2]]."','".$thatData[$otherData[3]]."','".$thatData[$otherData[4]]."','Nuevo','".$thatData[$otherData[5]]."','".$thatData[$otherData[6]]."','".$thatData[$otherData[7]]."','".$thatData[$otherData[8]]."','".$thatData[$otherData[9]]."','Pendiente','".$thatData[$otherData[12]]."','".$thatData[$otherData[13]]."','".$thatData[$otherData[14]]."','".$thatData[$otherData[15]]."')";
        $re = mysqli_query($conn,$sql);
        if(!$re)
          echo "error de conexion".mysqli_error();
        else
        {
          echo "<script type=\"text/javascript\">alert(\"Solicitud registrada exitosamente\");</script>";
          header('Location: http://'.$index);      
        }
      }
      else
        echo '<script type="text/javascript">alert("'.$fl.'");</script>';
      mysqli_close($conn);
    }
    function antihack($d)
    {
      $d = trim($d);
      $d = stripslashes($d);
      $d = htmlspecialchars($d);
      return $d;
    }
  ?>
</head>
<body>

 <div style="border:10px groove #616161;border-radius: 25px; ">
   <form method='POST' action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data"> <br>
     <div class="container2" align="center">
       <button type="submit"  name='iwantmachines' id='iwantmachines' >Registrar solicitud</button>
       <input type="reset" value="limpiar">
     </div>
     <br><br>
     <div class="line1">
     <?php
       $time= new DateTime();
       $time=$time->format('Y-m-d');
       echo "Fecha de recepción: <input type=\"text\" value=\"".$time."\" name=\"FR\" id=\"FR\" size=\"5\" disabled=\"true\" >";
       ?>
     </div>
     <div class="line2">
       <?php
        $sql = 'select FR2  from filtro order by FR2 desc limit 1';
        $conn = mysqli_connect($host,$user,$pass,$db);
        $re = mysqli_query($conn,$sql);
        if(!$re)
          echo "Conexion con BD fallida"; 
        else
        {
           $row = mysqli_fetch_array($re);
           echo "Folio recepción : <input type=\"text\" value=\"".($row['FR2']+1)."\" name=\"FR2\" id=\"FR2\" size=\"5\" disabled=\"true\" >";
        }
       ?>
     </div>
     <div class="line3">
        Proyecto : <input type="text" name="Pro" id="Pro" value="<?php echo $thatData['Pro']; ?>" autocomplete="off">
     </div>
     <br><br>
     <div class="line1">
       Ambiente Solicitado: <select name="ASol" id="ASol" >
         <option <?php if($thatData['ASol'] == ''){echo("selected");}?> value=""></option>
         <?php
          $re = mysqli_query($conn,"select nombre from ambiente");
          if(! $re)
            echo "<option value=\"Pendiente\">Pendiente</option> ";
          else
            while($row = mysqli_fetch_array($re))
            {
              $o ="<option ";
              if($thatData['ASol'] == $row['nombre'])
                $o.=" selected ";
              $o.="value=\"".$row['nombre']."\">".$row['nombre']."</option>";
              echo $o;
            }
        ?>
        </select>
     </div>
     <div class="line2">
       Solicitante : <input type="text" name="Sol" id="Sol" autocomplete="off" value="<?php echo $thatData['Sol']; ?>">
     </div>
     <div class="line3">
       Gerencia: <select name="Ger" id="Ger" >
        <option <?php if($thatData['Ger'] == ''){echo("selected");}?> value=""></option>
        <?php
          $re = mysqli_query($conn,"select nombre from gerencia");
          if(! $re)
            echo "<option value=\"Pendiente\">Pendiente</option> ";
          else
            while($row = mysqli_fetch_array($re))
            {
              $o ="<option ";
              if($thatData['Ger'] == $row['nombre'])
                $o.=" selected ";
              $o.="value=\"".$row['nombre']."\">".$row['nombre']."</option>";
              echo $o;
            }
        ?>
       </select>
     </div>
     <br><br>
     <div class="line1">
       Dirección: <select name="Dir" id="Dir">
       <option <?php if($thatData['Dir'] == ''){echo("selected");}?> value=""></option>
       <?php
         $re = mysqli_query($conn,"select nombre from direccion");
         if(! $re)
           echo "<option value=\"Pendiente\">Pendiente</option> ";
         else
          while($row = mysqli_fetch_array($re))
          {
            $o ="<option ";
            if($thatData['Dir'] == $row['nombre'])
              $o.=" selected ";
            $o.="value=\"".$row['nombre']."\">".$row['nombre']."</option>";
            echo $o;
          }
         mysqli_close($conn);
       ?>
       </select>
     </div>
     <div class="line2">
       Plataforma Nueva: <select name="PN" id="PN" disabled="yes" value="<?php echo $thatData['PN']; ?>">
          <option selected value="Si">Si</option>
        </select>
     </div>
     <div class="line3">
       Descripcion de proyecto :  <input type="text" name="DP" id="DP" autocomplete="off" value="<?php echo $thatData['DP']; ?>">
     </div>
     <br><br>
     <div class="line1">
       Proveedor de proyecto :  <input type="text" name="PP" id="PP" autocomplete="off" value="<?php echo $thatData['PP']; ?>">
     </div>
     <div class="line2">
       Tiempo implementación :  <input type="number" name="TEI" id="TEI" value="<?php echo $thatData['TEI']; ?>" size="5" autocomplete="off"  maxlength="3" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" >
     </div>
     <div class="line3">
       Diagrama de arquitectura :<input type="file" name="DA" id="DA">
     </div>
     <br><br>
     <div class="line1">
       Comentarios :  <input type="text" name="Com" id="Com" value="<?php echo $thatData['Com']; ?>" autocomplete="off">
     </div>
     <input type="button" name="NM" id="NM" onclick="window.open('theother.php','','menubar=0,titlebar=0,width=1280,height=400,resizable=0,left=40px,top=250px')" value="Meter Maquinas Virtuales" />
     <div class="line3">
       Status:<select  name="Sta" id="Sta"  disabled="yes" value="<?php echo $thatData['Sta']; ?>">
         <option selected value="Pendiente">Pendiente</option>
       </select>
     </div>
     <br><br>
     <div class="DaBox">
       <h4 style="margin-left:20%;">RIESGOS:<h4>
       <div style="margin-left:20px;">
       <br>
       Horario de operacion:
         <label for="Critica"> Critica : </label>
         <input type="radio" name="HO" id="Critica" value="Critica" <?php if($thatData['HO'] == 'Critica'){echo("checked");}?>>
         <label for="Alta"> Alta : </label>
         <input type="radio" name="HO" id="Alta" value="Alta" <?php if($thatData['HO'] == 'Alta'){echo("checked");}?>>
         <label for="Media"> Media : </label>
         <input type="radio" name="HO" id="Media" value="Media" <?php if($thatData['HO'] == 'Media'){echo("checked");}?>>
         <label for="Baja"> Baja : </label>
         <input type="radio" name="HO" id="Baja" value="Baja" <?php if($thatData['HO'] == 'Baja'){echo("checked");}?>>
       <br><br>
       No. Usuarios:
         <label for="Critica"> Critica : </label>
         <input type="radio" name="NU" id="Critica" value="Critica" <?php if($thatData['NU'] == 'Critica'){echo("checked");}?>>
         <label for="Alta"> Alta : </label>
         <input type="radio" name="NU" id="Alta" value="Alta" <?php if($thatData['NU'] == 'Alta'){echo("checked");}?>>
         <label for="Media"> Media : </label>
         <input type="radio" name="NU" id="Media" value="Media" <?php if($thatData['NU'] == 'Media'){echo("checked");}?>>
         <label for="Baja"> Baja : </label>
         <input type="radio" name="NU" id="Baja" value="Baja" <?php if($thatData['NU'] == 'Baja'){echo("checked");}?>>
       <br><br>
       Ingresos:
         <label for="Critica"> Critica : </label>
         <input type="radio" name="Ing" id="Critica" value="Critica" <?php if($thatData['Ing'] == 'Critica'){echo("checked");}?>>
         <label for="Alta"> Alta : </label>
         <input type="radio" name="Ing" id="Alta" value="Alta" <?php if($thatData['Ing'] == 'Alta'){echo("checked");}?>>
         <label for="Media"> Media : </label>
         <input type="radio" name="Ing" id="Media" value="Media" <?php if($thatData['Ing'] == 'Media'){echo("checked");}?>>
         <label for="Baja"> Baja : </label>
         <input type="radio" name="Ing" id="Baja" value="Baja" <?php if($thatData['Ing'] == 'Baja'){echo("checked");}?>>
       <br><br>
       Disponibilidad requerida:
         <label for="Critica"> Critica : </label>
         <input type="radio" name="DR" id="Critica" value="Critica" <?php if($thatData['DR'] == 'Critica'){echo("checked");}?>>
         <label for="Alta"> Alta : </label>
         <input type="radio" name="DR" id="Alta" value="Alta" <?php if($thatData['DR'] == 'Alta'){echo("checked");}?>>
         <label for="Media"> Media : </label>
         <input type="radio" name="DR" id="Media" value="Media" <?php if($thatData['DR'] == 'Media'){echo("checked");}?>>
         <label for="Baja"> Baja : </label>
         <input type="radio" name="DR" id="Baja" value="Baja" <?php if($thatData['DR'] == 'Baja'){echo("checked");}?>>
     </div>
     </div>
   </form>
   <button type="button" class="evilbtn">Tecnologias Cloud</button>
<br><br>
<p>   <p>
<br>
</div>
</body>
</html>